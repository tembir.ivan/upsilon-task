import React from "react";
import { InputAdornment, OutlinedInput } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";

type Props = {
  handleSearch: (event: React.ChangeEvent<HTMLInputElement>) => void;
};

const SearchInput = ({ handleSearch }: Props) => {
  return (
    <>
      <OutlinedInput
        fullWidth
        sx={{ marginTop: "12px" }}
        onChange={handleSearch}
        startAdornment={
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        }
      />
    </>
  );
};

export default SearchInput;

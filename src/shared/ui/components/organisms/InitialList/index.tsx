import React from "react";
import ShowMoreBox from "../../molecules/ShowMoreBox";
import useProducts from "../../../../lib/hooks/useProducts";
import ErrorLoadingIndicator from "../../molecules/ErrorLoadingIndicator";
import ProductList from "../../molecules/ProductList";

type Props = {
  search: string;
};

const InitialList = ({ search }: Props) => {
  const { products, loading, setLoadingProducts, error } = useProducts();

  if (loading || error)
    return <ErrorLoadingIndicator error={error} loading={loading} />;

  return (
    <>
      <ShowMoreBox handleShowMore={setLoadingProducts} />
      <ProductList products={products!} search={search} />
    </>
  );
};

export default InitialList;

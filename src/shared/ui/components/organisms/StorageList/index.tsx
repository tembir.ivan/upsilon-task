import React from "react";
import ProductList from "../../molecules/ProductList";
import useStorageProducts from "../../../../lib/hooks/useStorageProducts";

type Props = {
  search: string;
  published: boolean;
};

const StorageList = ({ search, published }: Props) => {
  const { products } = useStorageProducts();

  const filteredProducts = published
    ? products.filter((product) => product.published)
    : products.filter((product) => !product.published);

  return <ProductList products={filteredProducts} search={search} />;
};

export default StorageList;

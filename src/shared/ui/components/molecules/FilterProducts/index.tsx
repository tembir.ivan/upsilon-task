import {
  Box,
  Button,
  FormControlLabel,
  FormGroup,
  Link,
  Switch,
} from "@mui/material";
import React from "react";
import SearchInput from "../../atoms/SearchInput";

type Props = {
  handleSearch: (event: React.ChangeEvent<HTMLInputElement>) => void;
  changeListProducts: (event: React.MouseEvent<HTMLButtonElement>) => void;
  typeList: boolean;
  handlePublished: () => void;
  showPublished: boolean;
};

const FilterProducts = ({
  handleSearch,
  changeListProducts,
  typeList,
  showPublished,
  handlePublished,
}: Props) => {
  return (
    <Box marginTop={2}>
      <Link
        sx={{
          cursor: "pointer",
          border: "1px solid",
          ":hover": { borderColor: "darkblue" },
          transitionDuration: "0.3s",
        }}
        variant="button"
        padding={1}
        borderRadius={2}
        underline="none"
        href="/products/create"
      >
        Create New Product
      </Link>
      <SearchInput handleSearch={handleSearch} />
      <Box
        sx={{
          display: "flex",
          flexDirection: { sm: "row", xs: "column" },
          alignItems: { sm: "center", xs: "flex-start" },
          justifyContent: "space-between",
        }}
      >
        <Button onClick={changeListProducts}>
          {typeList ? "List of initial products" : "List of created products"}
        </Button>
        {typeList && (
          <FormGroup>
            <FormControlLabel
              control={
                <Switch checked={showPublished} onChange={handlePublished} />
              }
              label={showPublished ? "Show Published" : "Show Unpublished"}
            />
          </FormGroup>
        )}
      </Box>
    </Box>
  );
};

export default FilterProducts;

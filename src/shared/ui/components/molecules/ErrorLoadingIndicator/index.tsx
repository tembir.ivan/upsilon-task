import React from "react";
import { Alert, Typography } from "@mui/material";

type Props = {
  loading: boolean;
  error: string | undefined;
};

const ErrorLoadingIndicator = ({ loading, error }: Props) => {
  if (loading)
    return <Typography sx={{ marginTop: "12px" }}>Loading data...</Typography>;
  else
    return (
      <Alert sx={{ marginTop: "12px" }} severity={"error"}>
        {error}
      </Alert>
    );
};

export default ErrorLoadingIndicator;

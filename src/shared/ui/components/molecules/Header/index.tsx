import React from "react";
import { AppBar, Container, Link, Toolbar, Typography } from "@mui/material";
import useStorageProducts from "../../../../lib/hooks/useStorageProducts";

type Router = {
  path: string;
  name: string;
};

const pages: Router[] = [
  { name: "About", path: "https://gitlab.com/tembir.ivan" },
  { name: "Contact us", path: "https://www.linkedin.com/in/ivan-piskun" },
];

const Header = () => {
  useStorageProducts();

  return (
    <AppBar position="static">
      <Container
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Typography
          noWrap
          variant="h6"
          component="a"
          href="/products"
          sx={{
            fontWeight: 700,
            color: "#fff",
            textDecoration: "none",
          }}
        >
          Upsilon
        </Typography>
        <Toolbar
          component="nav"
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          {pages.map((page) => (
            <Link
              href={page.path}
              key={page.name}
              underline="none"
              color="#fff"
              target="_blank"
              sx={{
                ":first-of-type": { mr: 2 },
              }}
            >
              {page.name}
            </Link>
          ))}
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default Header;

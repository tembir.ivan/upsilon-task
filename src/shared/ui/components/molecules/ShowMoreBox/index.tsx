import React from "react";
import { Box, Button } from "@mui/material";

const buttons = [8, 16, 20];

type Props = {
  handleShowMore: (amount: number) => void;
};

const ShowMoreBox = ({ handleShowMore }: Props) => {
  return (
    <Box
      sx={{
        marginTop: 2,
        flexDirection: {
          sm: "row",
          xs: "column",
        },
        alignItems: { sm: "center", xs: "flex-start" },
      }}
      justifyContent="space-between"
      display="flex"
    >
      {buttons.map((amount) => (
        <Button
          variant="contained"
          key={amount}
          sx={{ marginTop: { sm: "0", xs: 1 } }}
          onClick={() => handleShowMore(amount)}
        >
          Load {amount} Products
        </Button>
      ))}
    </Box>
  );
};

export default ShowMoreBox;

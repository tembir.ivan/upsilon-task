import React from "react";
import { Grid } from "@mui/material";
import { Product } from "../../../../../entities/types";
import ProductBox from "../ProductBox";
import searchProducts from "../../../../lib/utils/searchProducts";

type Props = {
  products: Product[];
  search: string;
};

const ProductList = ({ products, search }: Props) => {
  const filteredProducts = searchProducts(products!, search);

  return (
    <Grid container marginTop={4} justifyContent="center" spacing={2}>
      {filteredProducts.map((product: Product) => (
        <Grid key={product.id} item xs={12} sm={6} md={4}>
          <ProductBox {...product} />
        </Grid>
      ))}
    </Grid>
  );
};

export default ProductList;

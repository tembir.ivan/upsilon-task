import React, { useState } from "react";
import {
  Alert,
  Box,
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  TextField,
} from "@mui/material";
import { useForm } from "react-hook-form";
import { Product } from "../../../../../entities/types";
import { useAppDispatch } from "../../../../lib/hooks/redux";
import {
  addProductFromForm,
  editProduct,
} from "../../../../../redux/products/productsSlice";
import createProduct from "../../../../lib/utils/createProduct";
import updateProduct from "../../../../lib/utils/updateProduct";

const Form = ({ id, description, title, price, published }: Product) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
  } = useForm<Product>({
    defaultValues: { title, description, price, published },
  });
  const dispatch = useAppDispatch();
  const [sendForm, setSendForm] = useState<boolean>(false);

  const onSubmit = async (data: Product) => {
    if (id) {
      const newProduct: Product = await updateProduct({ id, ...data });
      dispatch(editProduct(newProduct));
      setSendForm(true);
    } else {
      try {
        const newProduct: Product = (await createProduct(data)) as Product;
        dispatch(addProductFromForm(newProduct));
        setSendForm(true);
      } catch (error) {
        console.error(error);
        setError("root", { message: error as string });
      }
    }
  };

  if (errors.root)
    return <Alert severity="error">{errors.root?.message}</Alert>;

  return (
    <FormControl
      component="form"
      sx={{ width: "100%" }}
      onSubmit={handleSubmit(onSubmit)}
    >
      <FormGroup>
        <TextField
          {...register("title", { required: "Title is required" })}
          label="Title"
          fullWidth
          margin="normal"
          error={!!errors.title}
          helperText={errors.title?.message}
        />
        <TextField
          {...register("price", {
            required: "Price is required",
            pattern: {
              value: /^\d+(\.\d{1,2})?$/,
              message: "Price must be a number with up to two decimal places",
            },
          })}
          label="Price"
          type="number"
          fullWidth
          margin="normal"
          error={!!errors.price}
          helperText={errors.price?.message}
        />
        <TextField
          {...register("description", { required: "Description is required" })}
          label="Description"
          multiline
          fullWidth
          margin="normal"
          error={!!errors.description}
          helperText={errors.description?.message}
        />
        <FormControl fullWidth margin="normal">
          <FormControlLabel
            control={
              <Checkbox {...register("published")} defaultChecked={published} />
            }
            label="Published"
          />
        </FormControl>
        <Box textAlign="center">
          <Button type="submit" variant="contained" color="primary">
            {id ? "Update Product" : "Add Product"}
          </Button>
          {sendForm && <Alert sx={{ marginTop: 1 }}>Successfully!</Alert>}{" "}
        </Box>
      </FormGroup>
    </FormControl>
  );
};

export default Form;

import React from "react";
import { Product } from "../../../../../entities/types";
import {
  Card,
  CardContent,
  Typography,
  Menu,
  MenuItem,
  IconButton,
} from "@mui/material";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import DeleteConfirmation from "../DeleteConfirmation";
import useCardMenu from "../../../../lib/hooks/useCardMenu";

const ProductBox = ({
  id,
  title,
  image,
  price,
  published,
  description,
  timeCreated,
}: Product) => {
  const {
    openMenu,
    openDeleteDialog,
    handleCancelDelete,
    handleConfirmDelete,
    handleDelete,
    handleMenuOpen,
    handleMenuClose,
  } = useCardMenu(id!);

  return (
    <Card
      component="a"
      sx={{
        position: "relative",
        width: "100%",
        textDecoration: "none",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        height: "100%",
      }}
      href={`products/${id}`}
    >
      <CardContent>
        {image && (
          <img
            src={image}
            alt="Product"
            width="100%"
            height="150px"
            style={{ objectFit: "contain" }}
            loading="lazy"
          />
        )}
        <Typography marginTop={2} variant="h5" component="p">
          {title}
        </Typography>
        {published !== undefined && (
          <Typography variant="body2" color="text.secondary">
            {description}
          </Typography>
        )}
      </CardContent>
      <CardContent>
        {published !== undefined && (
          <Typography color="text.secondary" align="right">
            {published ? "Published" : "Not Published"}
          </Typography>
        )}
        <Typography color="text.secondary" align="right">
          Price: {price}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Time Created: {new Date(timeCreated!).toLocaleString()}
        </Typography>
      </CardContent>
      {published !== undefined && (
        <IconButton
          aria-label="more"
          aria-controls="product-menu"
          onClick={handleMenuOpen}
          sx={{
            position: "absolute",
            top: 0,
            right: 0,
          }}
        >
          <MoreVertIcon />
        </IconButton>
      )}
      <Menu
        anchorEl={openMenu}
        open={Boolean(openMenu)}
        onClose={handleMenuClose}
      >
        <MenuItem component="a" href={`products/edit/${id}`}>
          Edit
        </MenuItem>
        <MenuItem onClick={handleDelete}>Delete</MenuItem>
      </Menu>
      <DeleteConfirmation
        open={openDeleteDialog}
        onClose={handleCancelDelete}
        onConfirmDelete={handleConfirmDelete}
      />
    </Card>
  );
};

export default ProductBox;

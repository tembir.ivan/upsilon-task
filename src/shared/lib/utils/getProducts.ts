import { Product } from "../../../entities/types";

const getProducts = async (limit: number): Promise<Product[]> => {
  try {
    const response = await fetch(
      `https://fakestoreapi.com/products?limit=${limit}`
    );

    if (!response.ok) {
      throw new Error("Bad fetch response");
    }

    const data: Product[] = await response.json();
    return data;
  } catch (error) {
    console.error("Error fetching products:", error);
    return Promise.reject(error);
  }
};

export default getProducts;

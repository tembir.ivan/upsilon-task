import { Product } from "../../../entities/types";

const searchProducts = (products: Product[], search: string) => {
  return products?.filter(
    (product: Product | Product) =>
      product.title!.toLocaleLowerCase().includes(search) ||
      product.description!.toLocaleLowerCase().includes(search)
  );
};

export default searchProducts;

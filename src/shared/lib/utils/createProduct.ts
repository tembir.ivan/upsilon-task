import { Product } from "../../../entities/types";

const createProduct = async (data: Product) => {
  try {
    const repsonse = await fetch("https://fakestoreapi.com/products", {
      method: "POST",
      body: JSON.stringify(data),
    });

    if (!repsonse.ok) {
      throw new Error("Bad fetch response");
    }

    const storageProduct = localStorage.getItem("createProducts");
    let parseProducts = [];

    if (storageProduct) {
      parseProducts = JSON.parse(storageProduct!);
    }

    const createData = await repsonse.json();
    return {
      id: createData.id + parseProducts.length,
      timeCreated: new Date().getTime(),
      ...data,
    };
  } catch (error) {
    console.error(error);
    return error;
  }
};

export default createProduct;

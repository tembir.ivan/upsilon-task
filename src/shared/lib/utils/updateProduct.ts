import { Product } from "../../../entities/types";

const updateProduct = async (data: Product) => {
  try {
    const repsonse = await fetch(
      `https://fakestoreapi.com/products/${data.id}`,
      {
        method: "PUT",
        body: JSON.stringify(data),
      }
    );

    if (!repsonse.ok) {
      throw new Error("Bad fetch response");
    }

    const updateData = await repsonse.json();
    return { ...updateData, ...data };
  } catch (error) {
    console.error(error);
    return error;
  }
};

export default updateProduct;

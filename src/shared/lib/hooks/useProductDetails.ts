import { useEffect, useState } from "react";
import { Product } from "../../../entities/types";
import getProduct from "../utils/getProduct";
import { useAppSelector } from "./redux";

const useProductDetails = (id: string) => {
  const [product, setProduct] = useState<Product>();
  const [error, setError] = useState<string>();
  const [loading, setLoading] = useState<boolean>(true);
  const storageProducts = useAppSelector(
    (state) => state.products.productsFromForm
  );

  useEffect(() => {
    const storeProduct = storageProducts.find(
      (storeProduct: Product) => storeProduct.id === Number(id)
    );

    if (!storeProduct)
      getProduct(id)
        .then((listProducts: Product | null) => {
          if (listProducts !== null) setProduct(listProducts);
          else {
          }
          setLoading(false);
        })
        .catch((fetchError) => {
          setError(fetchError);
          setLoading(false);
        });
    else {
      setProduct(storeProduct);
      setLoading(false);
      setError(undefined);
    }
  }, [id, storageProducts]);

  return { product, error, loading };
};

export default useProductDetails;

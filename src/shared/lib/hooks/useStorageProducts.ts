import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "./redux";
import { loadProductsFromLocalStorage } from "../../../redux/products/productsSlice";

const useStorageProducts = () => {
  const products = useAppSelector((state) => state.products.productsFromForm);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(loadProductsFromLocalStorage());
  }, []);

  return { products };
};

export default useStorageProducts;

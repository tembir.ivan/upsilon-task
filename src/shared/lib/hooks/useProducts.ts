import { useEffect, useState } from "react";
import { Product } from "../../../entities/types";
import getProducts from "../utils/getProducts";
import { setStoreProducts } from "../../../redux/products/productsSlice";
import { useAppDispatch, useAppSelector } from "./redux";

const useProducts = () => {
  const [products, setProducts] = useState<Product[]>();
  const [error, setError] = useState<string>();
  const [loading, setLoading] = useState<boolean>(true);
  const [loadingProducts, setLoadingProducts] = useState<number>(8);
  const dispatch = useAppDispatch();
  const storeProducts = useAppSelector((state) => state.products);

  useEffect(() => {
    if (storeProducts.productsFromAPI.length !== loadingProducts) {
      getProducts(loadingProducts)
        .then((listProducts: Product[]) => {
          setProducts(listProducts);
          dispatch(
            setStoreProducts({
              productsFromAPI: listProducts,
              productsFromForm: storeProducts.productsFromForm,
            })
          );
          setLoading(false);
        })
        .catch((fetchError) => {
          setError(fetchError);
          setLoading(false);
        });
    } else {
      setProducts(storeProducts.productsFromAPI);
      dispatch(setStoreProducts(storeProducts));
      setLoading(false);
    }
  }, [loadingProducts]);

  return { products, error, loading, setLoadingProducts };
};

export default useProducts;

import { useState } from "react";

const useSearch = () => {
  const [search, setSearch] = useState<string>("");

  const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value.toLowerCase());
  };

  return { search, handleSearch };
};

export default useSearch;

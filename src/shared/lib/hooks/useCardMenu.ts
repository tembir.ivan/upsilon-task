import { useState } from "react";
import { useAppDispatch } from "./redux";
import { deleteProduct } from "../../../redux/products/productsSlice";

const useCardMenu = (id: number) => {
  const [openMenu, setOpenMenu] = useState<null | HTMLElement>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const dispatch = useAppDispatch();

  const handleMenuOpen = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    setOpenMenu(event.currentTarget);
  };

  const handleMenuClose = () => {
    setOpenMenu(null);
  };

  const handleDelete = () => {
    setOpenDeleteDialog(true);
    handleMenuClose();
  };

  const handleConfirmDelete = () => {
    dispatch(deleteProduct(id));
    setOpenDeleteDialog(false);
  };

  const handleCancelDelete = () => {
    setOpenDeleteDialog(false);
  };

  return {
    openMenu,
    openDeleteDialog,
    handleMenuOpen,
    handleDelete,
    handleConfirmDelete,
    handleCancelDelete,
    handleMenuClose,
  };
};

export default useCardMenu;

import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Product } from "../../entities/types";

type ProductsState = {
  productsFromAPI: Product[];
  productsFromForm: Product[];
};

const initialState: ProductsState = {
  productsFromAPI: [],
  productsFromForm: [],
};

export const productsSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    setStoreProducts: (state, action: PayloadAction<ProductsState>) => {
      state.productsFromAPI = action.payload.productsFromAPI;
      state.productsFromForm = action.payload.productsFromForm;
    },
    addProductFromForm: (state, action: PayloadAction<Product>) => {
      state.productsFromForm.push(action.payload);
      localStorage.setItem(
        "createProducts",
        JSON.stringify(state.productsFromForm)
      );
    },
    editProduct: (state, action: PayloadAction<Product>) => {
      const editedProductIndex = state.productsFromForm.findIndex(
        (product) => product.id === action.payload.id
      );
      if (editedProductIndex !== -1) {
        state.productsFromForm[editedProductIndex] = action.payload;
        localStorage.setItem(
          "createProducts",
          JSON.stringify(state.productsFromForm)
        );
      }
    },
    deleteProduct: (state, action: PayloadAction<number>) => {
      const index = state.productsFromForm.findIndex(
        (product) => product.id === action.payload
      );
      if (index !== -1) {
        state.productsFromForm.splice(index, 1);
        localStorage.setItem(
          "createProducts",
          JSON.stringify(state.productsFromForm)
        );
      }
    },
    loadProductsFromLocalStorage: (state) => {
      const localProducts = localStorage.getItem("createProducts");
      if (localProducts) {
        const parsedProducts = JSON.parse(localProducts);
        state.productsFromForm = parsedProducts;
      }
    },
  },
});

export const {
  setStoreProducts,
  addProductFromForm,
  editProduct,
  deleteProduct,
  loadProductsFromLocalStorage,
} = productsSlice.actions;
export default productsSlice.reducer;

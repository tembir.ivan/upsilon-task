import React, { useState } from "react";
import FilterProducts from "../../shared/ui/components/molecules/FilterProducts";
import useSearch from "../../shared/lib/hooks/useSearch";
import InitialList from "../../shared/ui/components/organisms/InitialList";
import StorageList from "../../shared/ui/components/organisms/StorageList";

const ProductsPage = () => {
  const { search, handleSearch } = useSearch();
  const [listCreated, setListCreaed] = useState<boolean>(false);
  const [showPublished, setShowPublished] = useState<boolean>(false);

  const handlePublished = () => {
    setShowPublished((e) => !e);
  };

  const changeList = () => {
    setListCreaed((e) => !e);
  };

  return (
    <>
      <FilterProducts
        handleSearch={handleSearch}
        changeListProducts={changeList}
        typeList={listCreated}
        handlePublished={handlePublished}
        showPublished={showPublished}
      />
      {listCreated ? (
        <StorageList search={search} published={showPublished} />
      ) : (
        <InitialList search={search} />
      )}
    </>
  );
};

export default ProductsPage;

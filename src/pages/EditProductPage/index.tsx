import React from "react";
import { Grid, Typography } from "@mui/material";
import { useParams } from "react-router-dom";
import Form from "../../shared/ui/components/molecules/Form";
import { useAppSelector } from "../../shared/lib/hooks/redux";

const EditProductPage = () => {
  const { id } = useParams();
  const product = useAppSelector(
    (state) => state.products.productsFromForm
  ).filter((storeProduct) => storeProduct.id === Number(id));

  return (
    <Grid container justifyContent="center" marginTop={4}>
      <Grid item xs={12} md={6}>
        <Typography variant="h4" align="center" gutterBottom>
          Edit Product
        </Typography>
        {product.length && <Form {...product[0]} />}
      </Grid>
    </Grid>
  );
};

export default EditProductPage;

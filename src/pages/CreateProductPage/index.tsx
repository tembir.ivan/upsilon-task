import React from "react";
import { Grid, Typography } from "@mui/material";
import Form from "../../shared/ui/components/molecules/Form";

const CreateProductPage = () => {
  return (
    <Grid container justifyContent="center" marginTop={4}>
      <Grid item xs={12} md={6}>
        <Typography variant="h4" align="center" gutterBottom>
          Create Product
        </Typography>
        <Form />
      </Grid>
    </Grid>
  );
};

export default CreateProductPage;

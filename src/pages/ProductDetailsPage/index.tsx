import React from "react";
import { useParams } from "react-router-dom";
import { Typography, Box } from "@mui/material";
import useProductDetails from "../../shared/lib/hooks/useProductDetails";
import ErrorLoadingIndicator from "../../shared/ui/components/molecules/ErrorLoadingIndicator";

const ProductDetailsPage = () => {
  const { id } = useParams();
  const { product, loading, error } = useProductDetails(id!);

  if (loading || error)
    return <ErrorLoadingIndicator loading={loading} error={error} />;

  if (!product) return <Typography>Product not found</Typography>;

  return (
    <Box
      marginTop={4}
      display="flex"
      justifyContent="space-between"
      sx={{
        flexDirection: { md: "row", xs: "column" },
        alignItems: { md: "flex-start", xs: "center" },
      }}
    >
      {product.image && (
        <Box
          sx={{
            width: { md: "500px", sm: "350px", xs: "100%" },
            height: { md: "400px", sm: "250px", xs: "150px" },
          }}
        >
          <img
            src={product.image}
            alt="Product"
            loading="lazy"
            width="100%"
            height="100%"
            style={{ objectFit: "contain" }}
          />
        </Box>
      )}
      <Box sx={{ margin: { md: "40px 0 0 40px", xs: "25px 0 0" } }}>
        <Typography variant="h5" component="h2">
          {product?.title}
        </Typography>
        <Typography color="text.secondary">Price: {product?.price}</Typography>
        <Typography color="text.secondary">
          Category: {product?.category}
        </Typography>
        <Typography variant="body2" component="p">
          Description: {product?.description}
        </Typography>
        {product.published !== undefined && (
          <>
            <Typography variant="body2" component="p">
              {product.published ? <>Published</> : <>Unpublished</>}
            </Typography>
          </>
        )}
      </Box>
    </Box>
  );
};

export default ProductDetailsPage;

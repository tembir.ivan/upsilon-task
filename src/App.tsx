import React from "react";
import {
  Navigate,
  RouterProvider,
  createBrowserRouter,
} from "react-router-dom";
import ProductsPage from "./pages/ProductsPage";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import "./app/styles/base/global.css";
import Header from "./shared/ui/components/molecules/Header";
import { Box, Container } from "@mui/material";
import ProductDetailsPage from "./pages/ProductDetailsPage";
import CreateProductPage from "./pages/CreateProductPage";
import EditProductPage from "./pages/EditProductPage";

const router = createBrowserRouter([
  { path: "/", element: <Navigate replace to="/products" /> },
  {
    path: "/products",
    element: <ProductsPage />,
  },
  {
    path: "products/:id",
    element: <ProductDetailsPage />,
    errorElement: <>This product is not in the database</>,
  },
  {
    path: "products/create",
    element: <CreateProductPage />,
  },
  {
    path: "products/edit/:id",
    element: <EditProductPage />,
  },
]);

function App() {
  return (
    <Provider store={store}>
      <Header />
      <Box component="main">
        <Box component="section">
          <Container
            sx={{ display: "flex", flexDirection: "column", paddingBottom: 3 }}
          >
            <RouterProvider router={router} />
          </Container>
        </Box>
      </Box>
    </Provider>
  );
}

export default App;
